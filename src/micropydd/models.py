from dataclasses import dataclass


@dataclass
class Logger:
    name: str
    level: str
