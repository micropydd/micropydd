from micropydd.config import Config


class MockConfig(Config):
    S3_BUCKET = 'mock-bucket'


def test_config():
    # Given
    config = MockConfig()

    # When
    version = config.MICROPYDD_VERSION

    # Then
    assert version is not None
