from dataclasses import dataclass
from typing import List

import pytest

from micropydd.utils import object_to_dict, dict_to_object


@dataclass
class MockObject:
    some_string_property: str
    some_int_property: int

    def some_method(self):
        return 'some_value'


@dataclass
class ComplexMockObject:
    complex: MockObject


@dataclass
class ListMockObject:
    list: List[MockObject]


def test_object_to_dict():
    # Given
    obj = MockObject(some_int_property=1, some_string_property='string')

    # When
    result = object_to_dict(obj)

    # Then
    assert result == {
        'some_string_property': 'string',
        'some_int_property': 1.
    }


def test_object_to_dict_complex():
    # Given
    obj = ComplexMockObject(
        complex=MockObject(some_int_property=1, some_string_property='string')
    )

    # When
    result = object_to_dict(obj)

    # Then
    assert result == {
        'complex': {
            'some_string_property': 'string',
            'some_int_property': 1.
        }
    }


def test_object_to_dict_list():
    # Given
    obj = ListMockObject(
        list=[MockObject(some_int_property=1, some_string_property='string')]
    )

    # When
    result = object_to_dict(obj)

    # Then
    assert result == {
        'list': [
            {
                'some_string_property': 'string',
                'some_int_property': 1.
            }
        ]
    }


def test_dict_to_object():
    # Given
    d = {
        'some_string_property': 'string',
        'some_int_property': 1.
    }

    # When
    result = dict_to_object(d, MockObject)

    # Then
    assert isinstance(result, MockObject)


def test_dict_to_object_unknown_attribute():
    # Given
    d = {
        'some_string_property': 'string',
        'some_int_property': 1.,
        'created_at': 1234
    }

    # Then
    with pytest.raises(KeyError):
        dict_to_object(d, MockObject, skip_unlisted_properties=False)


def test_dict_to_object_with_complex_object():
    # Given
    d = {
        'complex': {
            'some_string_property': 'string',
            'some_int_property': 1.
        }
    }

    # When
    result = dict_to_object(d, ComplexMockObject)

    # Then
    assert isinstance(result, ComplexMockObject)
    assert isinstance(result.complex, MockObject)


def test_dict_to_object_with_list():
    # Given
    d = {
        'list': [{
            'some_string_property': 'string',
            'some_int_property': 1.
        }]
    }

    # When
    result = dict_to_object(d, ListMockObject)

    # Then
    assert isinstance(result, ListMockObject)
    assert isinstance(result.list, list)
    assert isinstance(result.list[0], MockObject)
